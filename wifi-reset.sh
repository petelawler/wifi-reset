#!/bin/bash
# Bring all wifi interfaces down.
# Identify wifi interfaces as rows from standard output of iwconfig (NOT standard
# error, those are non-wifi interfaces) which start without whitespace.
DELAY=30
sleep $DELAY
connmanctl disable wifi
sleep $DELAY
# Bring all wifi interfaces up.
connmanctl enable wifi
for x in $(iwconfig 2> /dev/null| grep -o '^[[:alnum:]]\+'); do connmanctl session connect --ifname "$x"; done
