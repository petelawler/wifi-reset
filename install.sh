#!/bin/bash
echo "Installing wifi reset service to /opt/wifi-reset."
mkdir -pv /opt/wifi-reset
cp -fv wifi-reset.sh /opt/wifi-reset/wifi-reset.sh
echo "Installing systemd service to run at boot."
cp -fv wifi-reset.service /etc/systemd/system/wifi-reset.service
echo "Enabling systemd service."
systemctl enable wifi-reset.service
